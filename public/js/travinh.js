$(document).ready(function () {
  var video = document.getElementById("myAudio");
  $(".button a").click(function () {
    $(".overlay").fadeToggle(200);
    $(this).toggleClass('btn-open').toggleClass('btn-close');
  });


  $('#btnPlay').on('click', function () {
    if (video.paused) {
      video.play();
    } else {
      video.pause();
    }
  });

  $('#btnZoomIn').on('click', function () {
    video.width = 560;
  });
  $('#btnZoomOut').on('click', function () {
    video.width = 320;
  });
});

$('.overlay').on('click', function () {
  $(".overlay").fadeToggle(200);
  $(".button a").toggleClass('btn-open').toggleClass('btn-close');
  open = false;
});

