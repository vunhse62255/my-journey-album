var introduceImage = ['IntroduceImage/VungTau_1.jpg', 'IntroduceImage/SuoiMo_1.jpg', 'IntroduceImage/DaLat_1.jpg', 'IntroduceImage/DaLat_HTN'];
var carouselIndicator = [];
$(document).ready(() => {

  $("#myCarousel").carousel({ interval: 2000 });

  // swal({
  //   title: "Contact the owner to visit Đà Lạt 2017 journey",
  //   icon: "info",
  //   button: "OK"
  // });

  loadIndicator();
  $('.dropdown').on('show.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
  });

  $('.dropdown').on('hide.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
  });
  $("#header").load("header.html");
})

function loadIndicator() {
  carouselIndicator.push('<li data-target="#myCarousel" data-slide-to="' + 0 + '"class="active"></li>');
  for (var i = 1; i < 5; i++) {
    carouselIndicator.push('<li data-target="#myCarousel" data-slide-to="' + i + '"></li>');
  }
  $(".carousel-indicators").html(carouselIndicator.toString());
}
