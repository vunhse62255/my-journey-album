$(function () {
  $('.gallery_product').children().on('click', function () {
    var src = $(this).attr('src').slice(0, 15);
    var pic = $(this).attr('src').slice(22, $(this).attr('src').length);
    src = src.concat(pic)
    $('.enlargeImageModalSource').attr('src', src);
    // $('#enlargeImageModal').modal('show');
    swal({
      imageUrl: src,
      imageWidth: 640,
      imageHeight: 480,
      animation: false,
      showCancelButton: false,
      showConfirmButton: false,
      customClass: 'animated tada 3s'
    })
  });
});

window.onscroll = function () {
  scrollFunction()
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

$(document).ready(function () {
  $(".button a").click(function () {
    $(".overlay").fadeToggle(200);
    $(this).toggleClass('btn-open').toggleClass('btn-close');
  });
  $(".filter-button").click(function () {
    var value = $(this).attr('data-filter');

    if (value == "all") {
      $('.filter').show('1000');
    } else {
      $(".filter").not('.' + value).hide('3000');
      $('.filter').filter('.' + value).show('3000');
    }
  });

  if ($(".filter-button").removeClass("active")) {
    $(this).removeClass("active");
  } else {
    $(this).addClass("active");
  }
});

$('.overlay').on('click', function () {
  $(".overlay").fadeToggle(200);
  $(".button a").toggleClass('btn-open').toggleClass('btn-close');
  open = false;
});
