var express = require('express');
var router = express.Router();
const fs = require('fs');
const dir = './public/images/vung_tau/resize';
const imagePreDir = 'images/vung_tau/resize/';


/* GET home page. */
router.get('/', function (req, res, next) {

  fs.readdir(dir, (err, files) => {
    var images = [];
    files.forEach(element => {
      var image = { dir: imagePreDir + element.toString(), filter: element.substring(7, element.length - 6) };
      images.push(image);
    });

    res.render('vungtau', {
      images: images
    });
  });

});

module.exports = router;
